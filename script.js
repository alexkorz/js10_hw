

//Task 1

const newOptionnk = document.createElement('a');
newOptionnk.textContent = 'Learn more';
newOptionnk.setAttribute('href', '#');
newOptionnk.style.cssText = 'font: 12px; color: #596063; text-decoration: none';

const footerParagraph = document.querySelector('footer > p');
footerParagraph.after(newOptionnk);

//Task 2

const newSelect = document.createElement('select');
newSelect.id = 'rating';
newSelect.style.cssText = 'display: block; margin: 0 auto; margin-bottom: 20px;';

const options = ['4 Stars', '3 Stars', '2 Stars', '1 Star'];

options.forEach((el, id) => {
  const newOption = document.createElement('option');
  newOption.textContent = el;
  const optionsLength = options.length;
  newOption.value = JSON.stringify(optionsLength - id);
  newSelect.appendChild(newOption);
})

const features = document.querySelector('.features');
features.before(newSelect);











